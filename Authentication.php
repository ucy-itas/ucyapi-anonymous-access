<?php
namespace Iss\Api\Service\AnonymousAccess;

use Iss\Api\{ChainOfResponsibilityInterface, ChainOfResponsibilityTrait, ServiceInterface, ServiceTrait};
use Iss\Api\Authentication\Principal;
use Phalcon\Mvc\Micro;

class Authentication implements ServiceInterface, ChainOfResponsibilityInterface
{
    use ChainOfResponsibilityTrait;
    use ServiceTrait;

    private bool $_enabled = true;

    protected function process(array $request)
    {
        if (!$this->_enabled) {
            return null;
        }

        [$method] = $request;
        if (!is_null($method)) {
            return null;
        }

        return new Principal(["id" => 0]);
    }

    public function register(Micro $application) : bool
    {
        if (!$application->getDI()->has('authentication')) {
            // authorization service is not ready yet
            return false;
        }
        $application['authentication']->appendSuccessor($this);
        $this->_enabled = true;
        return true;
    }

    public function unregister(Micro $application) : ?ServiceInterface
    {
        $this->_enabled = false;
        return $this;
    }

    public static function getName() : string
    {
        return 'anonymous-access';
    }
}